vec4 pan = vec4(-0.7442895174026489, -9.351301621940422e-9, 0.12116021662950516, -2.252962441606776e-9);
vec3 colors = vec3(0.4313725531101227, 0.03921568766236305, 0.3137255012989044);

const float max_iterations = 10000.0;
const float escape_radius = 20.0;
const float max_scale = 60000.0;

// speed = -1 / (max_scale**(1/time)) + 1
const float time = 10.0;
// speed ~ 0.667199

uniform vec2 u_resolution;
uniform float u_time;

float aspect_ratio = u_resolution.x/u_resolution.y;

struct ComplexNumber{
    float real;
    float imag;
};

vec2 add(vec2 a, vec2 b){
    return vec2(a.x + b.x, 0.0);
}
vec2 mul(vec2 a, vec2 b){
	return vec2(a.x * b.x, 0.0);
}

// returns 1 if a > b
// returns 0 if a == b
// return -b if a < b
int cmp(vec2 a, vec2 b){
	if (a.x > b.x) return 1;
	else if (a.x == b.x){
		return 0;
	}
	else return -1;
}

ComplexNumber mul(ComplexNumber x, ComplexNumber y){
    return ComplexNumber(
		x.real * y.real - x.imag * y.imag,
		x.real * y.imag + x.imag * y.real
    );
}

ComplexNumber add(ComplexNumber x, ComplexNumber y){
    return ComplexNumber(
		x.real + y.real,
		x.imag + y.imag
    );
}

float mandelbrot(vec2 pos, vec2 pan, float scale){
	vec2 scaledPos = vec2(pos.x * scale, pos.y * scale);

    ComplexNumber c = ComplexNumber(scaledPos.x + pan.x, scaledPos.y + pan.y);
	ComplexNumber z = ComplexNumber(0.0, 0.0);
	for (float i = 0.0; i < max_iterations; i++){
		// if z.real**2 + z.imag**2 > escape_radius
		if (z.real * z.real + z.imag * z.imag > escape_radius) {
			return i + 1.0 - log(log(length(vec2(z.real, z.imag)))) / log(2.0);
		}
		z = mul(z, z);
		z = add(z, c);
	}
	return 0.0; // is stable
}

vec3 colorValue(float iterations) {
	return (-cos(colors*iterations) + 1.0) / 2.0;
}

void main() {
	float scale = pow(max_scale, -fract(u_time/time));
	/* float scale = pow(max_scale, 1.0-fract(u_time/time)) / max_scale; */
	vec2 pos = gl_FragCoord.xy/u_resolution*2.0 - 1.0;
	pos.x *= aspect_ratio;

	vec3 color = colorValue(mandelbrot(pos, pan.xz, scale));
	color += colorValue(mandelbrot(vec2(pos.x+0.001, pos.y+0.000), pan.xz, scale));
	color += colorValue(mandelbrot(vec2(pos.x+0.000, pos.y+0.001), pan.xz, scale));
	color += colorValue(mandelbrot(vec2(pos.x-0.001, pos.y-0.001), pan.xz, scale));
	color /= 4.0;
    gl_FragColor = vec4(color,1.0);
}
